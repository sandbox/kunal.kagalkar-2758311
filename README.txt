CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * USE
 * Maintainers

INTRODUCTION
---------------------------
Its basically designed for panel widgets. This module comes with form 
with title,description,image, and add/delete operation. 
Drag and drop sorting also available.

REQUIREMENTS
---------------------------
This module requires the following modules:

 * ctools (https://drupal.org/project/ctools)
 * Panels (https://drupal.org/project/panels)
 * Imce (https://drupal.org/project/imce)
 * Bootstrap (https://drupal.org/project/bootstrap)


USE
---------------------------
For using this module create page from page manager and add
bootstrap banner widget to any region with data on form.

MAINTAINERS
---------------------------
Current maintainers:
Kunal Kagalkar - https://drupal.org/user/kunal.kagalkar
