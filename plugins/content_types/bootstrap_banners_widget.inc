<?php

/**
 * @file
 * This is defined for widget information.
 */

/**
 * Plugin definition.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Bootstrap Banners Widget'),
  'description' => t('Select Banners for this widget'),
  'category' => t('Widgets'),
  'top level' => FALSE,
  'render callback' => 'bootstrap_banners_widget_render',
  'edit form' => 'bootstrap_banners_widget_form',
  'defaults' => array(),
  'all contexts' => TRUE,
);

/**
 * Front End Render.
 */
function bootstrap_banners_widget_render($subtype, $conf, $panel_args, $context) {
  if (!$conf) {
    return bootstrap_banners_widget_info($subtype, $conf, $panel_args, $context);
  }
  $block = new stdClass();
  $block->content = theme($conf['display_format'], array(
    'nodes' => $conf['nodes'],
  ));
  return $block;
}

/**
 * Admin Widget Selection Render.
 */
function bootstrap_banners_widget_info($subtype, $conf, $panel_args, $context) {
  global $base_url;
  $block = new stdClass();
  $block->title = t('Bootstrap Banners Widget');
  $block->content = "<img src='" . $base_url . '/' . drupal_get_path('module', 'bootstrap_banners_widget') . "/bootstrap_banners_widget.png' style='width: 100%;' />";
  return $block;
}
