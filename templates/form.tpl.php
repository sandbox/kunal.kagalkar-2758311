<?php

/**
 * @file
 * Admin form.
 */
if (@!$nodes) {
    $nodes = array();
}
?>
<input type="button" id="node-add" value="Add a new row" />
<table id="node-table" class="table table-striped table-bordered">
    <thead>
      <tr>
        <th width="30"></th>                      
        <th>Contents</th>
        <th width="100">Image</th>
        <th>Link</th>
        <th width="100" class="text-center">Operations</th>
      </tr>
    </thead>
    <tbody>      
      <tr class="empty-row">
        <td colspan="5"><strong>No selected data yet.</strong><br />Start by searching and adding rows</td>
       </tr>      
   </tbody>
</table>
