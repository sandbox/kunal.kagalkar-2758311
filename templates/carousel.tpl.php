<?php

/**
 * @file
 * Carosuel loop.
 */

$i = 0;
?>
<!-- Start Bootstrap  -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php
    foreach ($nodes as $node):
      if($i == 0) {
        $active = "active";
      }else{
         $active = "";
      }
    ?>
    <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $active; ?>"></li>
   <?php
    $i++;
    endforeach;
   ?> 
  </ol>

  <!-- Wrapper for slides -->
  <?php $i = 1; ?>
  <div class="carousel-inner" role="listbox">
     <?php foreach ($nodes as $node):
      if($i == 1) {
        $active = "active";
      }else{
        $active = "";
      }
     ?>
      <div class="item <?php echo $active; ?>">    
      <div class="jumbotron" style="background-image: url(<?php echo $node->image; ?>);">
      <div class="container-fluid">           
      <h2><?php print check_plain($node->title); ?></h2>
        <p><?php echo check_plain($node->body); ?></p>
        <p><a class="btn btn-primary btn-lg" href="<?php echo check_plain($node->link); ?>" role="button"><?php print t("Learn more");?></a></p>      
      </div>    
     </div>     
     </div>
  <?php
     $i++;
     endforeach;
  ?> 
  </div>
</div>
<!-- End Bootstrap -->
<style type="text/css">
   .jumbotron {
    margin-bottom: 0px;  
    background-position: 0% 25%;
    background-size: cover;
    background-repeat: no-repeat;
    color: white;
    text-shadow: black 0.3em 0.3em 0.3em;
}
</style>
