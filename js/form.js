/**
 * @file
 * Form js for editing slider content.
 */
Drupal.behaviors.bootstrap_banners_widget = {
  attach: function(context, settings) {
    jQuery(function() {
      if (typeof jQuery.fn.sortable === 'function') {
        jQuery("#node-table tbody").sortable({
          axis: "y",
          handle: ".drag-handle",
          helper: function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
              jQuery(this).width($originals.eq(index).width());
            });
            return $helper;
          }
        });
      };
      jQuery("#node-add").click(function(e) {
        e.preventDefault();
        var id = bootstrap_banners_widget_addRow({
          stat: '',
          unit: ''
        });
      });
      var conf_data = Drupal.settings.bootstrap_banners_widget.nodes;
      for (var i = 0; i < conf_data.length; i++) {
        var id = bootstrap_banners_widget_addRow(conf_data[i]);
      }
      jQuery("#settings-form #edit-buttons-return").on("click", function(e) {
        jQuery('#node-table textarea').each(function() {
          var input = $(this);
          var id = input.attr('id');
        });
      });
      function bootstrap_banners_widget_addRow(row) {
        var id = Math.floor((1 + Math.random()) * 0x100000000).toString(16).substring(1);
        var $new_row = jQuery('<tr id="row-' + id + '">' + '<td><span class="drag-handle"></span></td>' +
          '<td><input type="text" class="mbot20 large" placeholder="Optional Title" name="title[' + id + ']" value="' + (row.title ? row.title : '') +
          '" />' + '<textarea class="large ckeditor" cols="40" rows="10" id="editor-' + id + '" name="body[' + id + ']">' + (row.body ? row.body : '') +
          '</textarea></td>' + '<td class="text-center"><input type="hidden" id="image-' + id + '" name="image[' + id + ']" value="' + (row.image ?
            row.image : '') + '" />' + '<img src="' + row.image + '" class="mbot20 preview ' + (row.image ? '' : 'empty') + ' image-' + id + '" />' +
          '<input type="button" name="browse[' + id + ']" value="Browse Server" onclick="bootstrap_banners_widget_openFileBrowser(\'' + id +
          '\');"/>' + '<a href="#" id="image-button-' + id + '" class="node-remove-image">Remove Image</a></td>' +
          '<td class="text-center"><input type="text" name="link[' + id + ']" value="' + (row.link ? row.link : '') + '" /></td>' +
          '<td class="text-center"><a href="#" class="node-delete">Delete</a></td>' + '</tr>');
        jQuery("#node-table tbody").append($new_row);
        jQuery("#row-" + id + " .node-delete").click(function(e) {
          e.preventDefault();
          $tr = jQuery('#row-' + id);
          $tr.fadeOut(400, function() {
            $tr.remove();
            if (jQuery("#node-table tbody TR").length == 1) {
              jQuery("#node-table tbody TR.empty-row").show();
            }
          });
        });
        jQuery("#row-" + id + " .node-remove-image").click(function(e) {
          e.preventDefault();
          jQuery('#image-' + id).val('');
          jQuery('.image-' + id).attr('src', '').addClass('empty');
        });
        jQuery("#node-table tbody TR.empty-row").hide();
        if (typeof jQuery.fn.sortable === 'function') {
          jQuery("#node-table tbody").sortable("refresh");
        }
        return id;
      }
    });
  }
};
var fileId = "";

function bootstrap_banners_widget_openFileBrowser(arg) {
  fileId = arg;
  window.open('?q=imce&app=ckeditor|sendto@bootstrap_banners_widget_imageHandler', '', 'width=760,height=560,resizable=1');
}

function bootstrap_banners_widget_imageHandler(file, win) {
  jQuery('#image-' + fileId).val(file.url);
  jQuery('.image-' + fileId).attr('src', file.url).removeClass('empty');
  win.close();
}
